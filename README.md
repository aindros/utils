# c-utils

A utility toolkit for the C language. These are the main features:

* lists - you can manage lists in a simple way.

Upcoming features:

* maps - you can create maps with a key ad a value. The only limitation is the
key must be a string.
